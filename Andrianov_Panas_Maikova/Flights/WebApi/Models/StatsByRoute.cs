﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class FlightStats
    {
        public double AvgDelay;
        public int MinDelay;
        public int MaxDelay;
        public int DelayedCount;
        public double DelayedPercent;
        public int CancelledCount;
        public double CancelledPercent;
        public int DivertedCount;
        public double DivertedPercent;
        public int TotalCount;

        public FlightStats(double avgDelay, int minDelay, int maxDelay, int cancelledCount, int divertedCount, int totalCount, int delayedCount)
        {
            AvgDelay = avgDelay;
            MinDelay = minDelay;
            MaxDelay = maxDelay;
            CancelledCount = cancelledCount;
            DivertedCount = divertedCount;
            TotalCount = totalCount;
            DelayedCount = delayedCount;

            if (TotalCount > 0)
            {
                DelayedPercent = (Convert.ToDouble(DelayedCount) / TotalCount) * 100;
                CancelledPercent = (Convert.ToDouble(CancelledCount) / TotalCount) * 100;
                DivertedPercent = (Convert.ToDouble(DivertedCount) / TotalCount) * 100;
            }
        }
    }
}