﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class Flight
    {
        public string Airline;
        public string FlightNumber;
        public string TailNumber;
        public string OriginAirport;
        public string DestinationAirport;
        public FlightStats Stats { get; set; }

        public Flight(string airline, string flight_number, string tail_number, string origin_airport, string destination_airport, FlightStats stats)
        {
            this.Airline = airline;
            this.FlightNumber = flight_number;
            this.TailNumber = tail_number;
            this.OriginAirport = origin_airport;
            this.DestinationAirport = destination_airport;
            this.Stats = stats;
        }
    }
}