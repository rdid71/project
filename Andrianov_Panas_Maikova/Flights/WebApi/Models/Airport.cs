﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class Airport
    {
        public string AirportCode { get; set; }

        public Airport(string airportCode)
        {
            this.AirportCode = airportCode;
        }
    }
}