﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebApi
{
    public static class WebApiConfig
    {
        public static MySqlConnection conn()
        {
            //string conn_string = "server=localhost;port=3306;database=books_database;username=root;password=;";

            string conn_string = "Server=35.228.143.33;Database=flightsDB;Uid=root;Pwd=;";

            MySqlConnection conn = new MySqlConnection(conn_string);

            return conn;
        }

        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.EnableCors();
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "ApiToGetAllFormData",
                routeTemplate: "api/{controller}/{action}/{origin}/{destination}/{From}/{Until}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
