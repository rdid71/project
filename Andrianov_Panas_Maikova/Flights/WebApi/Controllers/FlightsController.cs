﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Models;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FlightsController : ApiController
    {

        //http://localhost:6235/api/values/getOriginAirports
        //gets all airports names to fill into list on main page
        [HttpGet]
        public List<Airport> GetOriginAirports()
        {
            MySqlConnection conn = WebApiConfig.conn();
            MySqlCommand query = conn.CreateCommand();
            query.CommandText = "SELECT DISTINCT origin_airport FROM dataset";
            var results = new List<Airport>();

            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                results.Add(new Airport(ex.ToString()));
            }

            MySqlDataReader fetch_query = query.ExecuteReader();

            while (fetch_query.Read())
            {
                results.Add(new Airport(fetch_query["origin_airport"].ToString()));
            }

            return results;
        }

        //http://localhost:6235/api/values/getOriginAirports
        //gets all airports names to fill into list on main page
        [HttpGet]
        public List<Airport> GetOriginAirportsByCodePart(string searchText)
        {
            MySqlConnection conn = WebApiConfig.conn();
            MySqlCommand query = conn.CreateCommand();
            query.CommandText = "SELECT DISTINCT origin_airport FROM dataset";
            if (searchText != null) query.CommandText += string.Format(" WHERE origin_airport LIKE '{0}%'", searchText);
            var results = new List<Airport>();

            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                results.Add(new Airport(ex.ToString()));
            }

            MySqlDataReader fetch_query = query.ExecuteReader();

            while (fetch_query.Read())
            {
                results.Add(new Airport(fetch_query["origin_airport"].ToString()));
            }

            return results;
        }

        //http://localhost:6235/api/values/getOriginAirports
        //gets all airports names to fill into list on main page
        [HttpGet]
        public List<Airport> GetDestinationAirportsByOrigin(string originAirportCode)
        {
            MySqlConnection conn = WebApiConfig.conn();
            MySqlCommand query = conn.CreateCommand();
            query.CommandText = string.Format("SELECT DISTINCT destination_airport FROM dataset WHERE origin_airport LIKE '{0}'", originAirportCode);
            var results = new List<Airport>();

            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                results.Add(new Airport(ex.ToString()));
            }

            MySqlDataReader fetch_query = query.ExecuteReader();

            while (fetch_query.Read())
            {
                results.Add(new Airport(fetch_query["destination_airport"].ToString()));
            }

            return results;
        }

        //http://localhost:6235/api/values/getDestinationAirports
        //gets all airports names to fill into list on main page
        [HttpGet]
        public List<Airport> GetDestinationAirports()
        {
            MySqlConnection conn = WebApiConfig.conn();
            MySqlCommand query = conn.CreateCommand();
            query.CommandText = "SELECT DISTINCT destination_airport FROM dataset";
            var results = new List<Airport>();

            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                results.Add(new Airport(ex.ToString()));
            }

            MySqlDataReader fetch_query = query.ExecuteReader();

            while (fetch_query.Read())
            {
                results.Add(new Airport(fetch_query["destination_airport"].ToString()));
            }

            return results;
        }
        
        //http://localhost:6235/api/values/GetDelayStats/LAX/MSP/1/1
        [HttpGet]
        public List<FlightStats> GetDelayStats(string origin, string destination, string tail_number, int from, int until)
        {
            MySqlConnection conn = WebApiConfig.conn();
            MySqlCommand query = conn.CreateCommand();

            query.CommandText = "SELECT COUNT(arrival_delay) AS totalCount, SUM(CANCELLED) AS cancelledCount, SUM(DIVERTED) AS divertedCount, AVG(arrival_delay) AS avgDelay, MIN(arrival_delay) AS minDelay, MAX(arrival_delay) AS maxDelay  FROM dataset WHERE month BETWEEN '" + from + "' AND '" + until + "' AND origin_airport = '" + origin + "' AND destination_airport = '" + destination + "' AND tail_number = '" + tail_number + "';";
            var results = new List<FlightStats>();

            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                results.Add(new FlightStats(0,0,0,0, 0, 0,0));
            }

            MySqlDataReader fetch_query = query.ExecuteReader();


            while (fetch_query.Read())
            {
                results.Add(new FlightStats(
                    Double.Parse(fetch_query["avgDelay"].ToString()),
                        Int32.Parse(fetch_query["minDelay"].ToString()),
                        Int32.Parse(fetch_query["maxDelay"].ToString()),
                        Int32.Parse(fetch_query["cancelledCount"].ToString()),
                        Int32.Parse(fetch_query["divertedCount"].ToString()),
                        Int32.Parse(fetch_query["totalCount"].ToString()),
                        Int32.Parse(fetch_query["delayedCount"].ToString())));
            }

            return results;
        }
        
        //http://localhost:6235/api/values/GetBestFlight/LAX/MSP/1/1/2/2
        public List<Flight> GetBestFlight(string origin, string destination, int fromMonthSpan1, int UntilMonthSpan1, int fromMonthSpan2, int untilMonthSpan2)
        {

            MySqlConnection conn = WebApiConfig.conn();
            MySqlCommand query = conn.CreateCommand();
            if(fromMonthSpan2 == 0 || untilMonthSpan2 == 0)
            query.CommandText = "SELECT d.ARRIVAL_DELAY, d.DIVERTED, d.CANCELLED, d.MONTH, d.DAY, d.AIRLINE, d.FLIGHT_NUMBER, d.TAIL_NUMBER, d.ORIGIN_AIRPORT, d.DESTINATION_AIRPORT, COUNT(d2.arrival_delay) AS totalCount, SUM(d2.CANCELLED) AS cancelledCount, SUM(d2.DIVERTED) AS divertedCount, AVG(d2.arrival_delay) AS avgDelay, MIN(d2.arrival_delay) AS minDelay, MAX(d2.arrival_delay) AS maxDelay " +
                "FROM dataset d " +
                "INNER JOIN dataset d2 ON d.tail_number = d2.tail_number " + " AND d2.ORIGIN_AIRPORT = d.ORIGIN_AIRPORT AND d2.DESTINATION_AIRPORT = d.DESTINATION_AIRPORT AND d2.month BETWEEN '" + fromMonthSpan1 + "' AND '" + UntilMonthSpan1 + "' " +
                "WHERE d.month BETWEEN '" + fromMonthSpan1 + "' AND '" + UntilMonthSpan1 + "' AND d.origin_airport = '" + origin + "' " +
                "AND d.destination_airport = '" + destination + "' AND d.arrival_delay = (SELECT MIN(arrival_delay) FROM dataset d3 " +
                "WHERE d3.month BETWEEN '" + fromMonthSpan1 + "' AND '" + UntilMonthSpan1 + "' AND d3.origin_airport = '" + origin + "' AND d3.destination_airport = '" + destination + "')" +
                " GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10;";
            else
                query.CommandText = "SELECT d.ARRIVAL_DELAY, d.DIVERTED, d.CANCELLED, d.MONTH, d.DAY, d.AIRLINE, d.FLIGHT_NUMBER, d.TAIL_NUMBER, d.ORIGIN_AIRPORT, d.DESTINATION_AIRPORT, COUNT(d2.arrival_delay) AS totalCount, SUM(d2.CANCELLED) AS cancelledCount, SUM(d2.DIVERTED) AS divertedCount, AVG(d2.arrival_delay) AS avgDelay, MIN(d2.arrival_delay) AS minDelay, MAX(d2.arrival_delay) AS maxDelay " +
                "FROM dataset d " +
                "INNER JOIN dataset d2 ON d.tail_number = d2.tail_number " + " AND d2.ORIGIN_AIRPORT = d.ORIGIN_AIRPORT AND d2.DESTINATION_AIRPORT = d.DESTINATION_AIRPORT AND d2.month BETWEEN '" + fromMonthSpan1 + "' AND '" + UntilMonthSpan1 + "' " +
                "WHERE d.month BETWEEN '" + fromMonthSpan1 + "' AND '" + UntilMonthSpan1 + "' AND d.origin_airport = '" + origin + "' " +
                "AND d.destination_airport = '" + destination + "' AND d.arrival_delay = (SELECT MIN(arrival_delay) FROM dataset d3 " +
                "WHERE d3.month BETWEEN '" + fromMonthSpan1 + "' AND '" + UntilMonthSpan1 + "' AND d3.origin_airport = '" + origin + "' AND d3.destination_airport = '" + destination + "')" +
                " GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10;";

            var results = new List<Flight>();

            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                results.Add(new Flight(ex.ToString(),"","","","", null));
            }

            MySqlDataReader fetch_query = query.ExecuteReader();

            while (fetch_query.Read())
            {
                results.Add(new Flight(fetch_query["airline"].ToString(),
                    fetch_query["flight_number"].ToString(), 
                    fetch_query["tail_number"].ToString(), 
                    fetch_query["origin_airport"].ToString(),
                    fetch_query["destination_airport"].ToString(), 
                    new FlightStats(
                        Double.Parse(fetch_query["avgDelay"].ToString()),
                        Int32.Parse(fetch_query["minDelay"].ToString()),
                        Int32.Parse(fetch_query["maxDelay"].ToString()),
                        Int32.Parse(fetch_query["cancelledCount"].ToString()),
                        Int32.Parse(fetch_query["divertedCount"].ToString()),
                        Int32.Parse(fetch_query["totalCount"].ToString()),
                        Int32.Parse(fetch_query["delayedCount"].ToString())
                        )
                    ));
            }

            return results;
        }

        //http://localhost:6235/api/values/GetBestFlight/LAX/MSP/1/1/2/2
        public List<Flight> GetFlights(string origin, string destination, int fromMonthSpan1, int untilMonthSpan1, int fromMonthSpan2, int untilMonthSpan2)
        {

            MySqlConnection conn = WebApiConfig.conn();
            MySqlCommand query = conn.CreateCommand();
            query.Parameters.AddWithValue("fromMonthSpan1", fromMonthSpan1);
            query.Parameters.AddWithValue("untilMonthSpan1", untilMonthSpan1);
            query.Parameters.AddWithValue("origin", origin);
            query.Parameters.AddWithValue("destination", destination);
            if (fromMonthSpan2 == 0 || untilMonthSpan2 == 0)
            {
                query.CommandText =
                        "SELECT d.AIRLINE, d.FLIGHT_NUMBER, d.ORIGIN_AIRPORT, d.DESTINATION_AIRPORT, " +
                            "COUNT(d.arrival_delay) AS totalCount, SUM(CASE WHEN d.arrival_delay > 0 THEN 1 ELSE 0 END) AS delayedCount, SUM(d.CANCELLED) AS cancelledCount, SUM(d.DIVERTED) AS divertedCount, " +
                            "AVG(d.arrival_delay) AS avgDelay, MIN(d.arrival_delay) AS minDelay, MAX(d.arrival_delay) AS maxDelay " +
                        "FROM dataset d " +
                        "WHERE d.month BETWEEN @fromMonthSpan1 AND @untilMonthSpan1 AND d.origin_airport = @origin " +
                            "AND d.destination_airport = @destination GROUP BY 1, 2, 3, 4 ORDER BY avgDelay DESC;";
            }
            else {
                query.Parameters.AddWithValue("fromMonthSpan2", fromMonthSpan2);
                query.Parameters.AddWithValue("untilMonthSpan2", untilMonthSpan2);
                query.CommandText =
                        "SELECT d.AIRLINE, d.FLIGHT_NUMBER, d.ORIGIN_AIRPORT, d.DESTINATION_AIRPORT, " +
                            "COUNT(d.arrival_delay) AS totalCount, SUM(CASE WHEN d.arrival_delay > 0 THEN 1 ELSE 0 END) AS delayedCount, SUM(d.CANCELLED) AS cancelledCount, SUM(d.DIVERTED) AS divertedCount, " +
                            "AVG(d.arrival_delay) AS avgDelay, MIN(d.arrival_delay) AS minDelay, MAX(d.arrival_delay) AS maxDelay " +
                        "FROM dataset d " +
                        "WHERE (d.month BETWEEN @fromMonthSpan1 AND @untilMonthSpan1 OR d.month BETWEEN @fromMonthSpan2 AND @untilMonthSpan2) AND d.origin_airport = @origin " +
                            "AND d.destination_airport = @destination GROUP BY 1, 2, 3, 4 ORDER BY avgDelay DESC;";
            }

            var results = new List<Flight>();

            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                results.Add(new Flight(ex.ToString(), "", "", "", "", null));
            }

            MySqlDataReader fetch_query = query.ExecuteReader();

            while (fetch_query.Read())
            {
                results.Add(new Flight(fetch_query["airline"].ToString(),
                    fetch_query["flight_number"].ToString(),
                    "",
                    fetch_query["origin_airport"].ToString(),
                    fetch_query["destination_airport"].ToString(),
                    new FlightStats(
                        Double.Parse(fetch_query["avgDelay"].ToString()),
                        Int32.Parse(fetch_query["minDelay"].ToString()),
                        Int32.Parse(fetch_query["maxDelay"].ToString()),
                        Int32.Parse(fetch_query["cancelledCount"].ToString()),
                        Int32.Parse(fetch_query["divertedCount"].ToString()),
                        Int32.Parse(fetch_query["totalCount"].ToString()),
                        Int32.Parse(fetch_query["delayedCount"].ToString())
                        )
                    ));
            }

            return results.OrderBy(f => f.Stats.AvgDelay).ToList();
        }

    }
}
