import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

export interface Flight {
  airline: string;
  flight_number: string;
}

export interface Airport {
  AirportCode: string;
}

@Injectable()
export class HomeService {
  constructor(private http: HttpClient) { }

  apiUrl = 'http://localhost:6235/api/flights';

  getFlight() {
    // now returns an Observable of Config
    return this.http.get<Flight[]>(this.apiUrl+'/getfromdb/10');
  }

  getOriginAirports() {
    // now returns an Observable of Config
    return this.http.get<Airport[]>(this.apiUrl+'/getoriginairports');
  }

  getOriginAirportsByCodePart(codePart) {
    // now returns an Observable of Config
    return this.http.get<Airport[]>(this.apiUrl+'/GetOriginAirportsByCodePart?searchText=' + codePart);
  }

  getDestinationAirports() {
    // now returns an Observable
    return this.http.get<Airport[]>(this.apiUrl+'/getdestinationairports');
  }

  getDestinationsByOrigin(origin) {
    // now returns an Observable
    return this.http.get<Airport[]>(this.apiUrl+'/GetDestinationAirportsByOrigin?originAirportCode=' + origin);
  }

}
