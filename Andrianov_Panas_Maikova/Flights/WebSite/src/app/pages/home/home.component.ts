import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Flight, Airport, HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [ HomeService ],
  styleUrls: ['./home.component.css']
})

export class HomeComponent {
  
  flights: Flight[];
  origins: Airport[];
  destinations: Airport[];
  originAirport: Airport;
  destinationAirport: Airport;
  dateFrom: any;
  dateUntil: any;

  currDate: Date;
  maxDate: Date;
  currDateString: string;
  maxDateString: string;

  constructor(private flightService: HomeService, private route: ActivatedRoute,
    private router: Router,) {  
      this.currDate = new Date();
      this.currDateString = this.currDate.toDateString();
      this.maxDate = new Date();
      this.maxDate.setFullYear(this.currDate.getFullYear() + 1);
      this.maxDateString = "2020-01-12";
      this.currDateString = "2019-01-12";
  }

  onSubmit(f){
    if(this.originAirport && this.destinationAirport && this.dateFrom && this.dateUntil){      
      this.goToFlightsInfo(this.originAirport, this.destinationAirport,
      this.dateFrom.toString(),this.dateUntil.toString());
    }
  }

  dateFromChanged(value){
    this.dateFrom = value;
  }

  dateUntilChanged(value){
    this.dateUntil = value;
  }

  originChanged(value){
    this.getDestinationsByOrigin(value);
  }

  originSearch(value){
    if(value != null && value.length < 2 && value.length > 0)
    this.getOriginsByCodePart(value);
  }

  destChanged(value){
  }

getOrigins(){
  this.flightService.getOriginAirports()
    .subscribe((data: Airport[]) => this.origins = data );
}

getOriginsByCodePart(codePart){
  this.flightService.getOriginAirportsByCodePart(codePart)
    .subscribe((data: Airport[]) => this.origins = data );
}

getDestinationsByOrigin(origin){
  this.flightService.getDestinationsByOrigin(origin)
    .subscribe((data: Airport[]) => this.destinations = data );
}

getDestinations(){
  this.flightService.getDestinationAirports()
    .subscribe((data: Airport[]) => this.destinations = data );
}

goToFlightsInfo(orig, dest, from, until){
  this.router.navigate(['/flights', { orig: orig, dest: dest, from: from, until: until }]);
}

}
