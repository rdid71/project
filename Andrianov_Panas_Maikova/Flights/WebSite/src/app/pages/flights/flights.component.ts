import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FlightsService, Flight } from "./flights.service";

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit {
  flightsNotFound: boolean;
  loading: boolean;

  origin: string;
  dest: string;
  from: string;
  until: string;

  bestFlight: Flight;
  worstFlight: Flight;
  flightsList: Flight[];

  constructor(private route: ActivatedRoute, private service: FlightsService) { 
    this.bestFlight = null;
    this.worstFlight = null;
    this.flightsList = null;
    this.flightsNotFound = false;
    this.loading = true;
  }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.origin = paramMap.get("orig");
      this.dest = paramMap.get("dest");
      this.from = paramMap.get("from");
      this.until = paramMap.get("until");

      if(!isNaN(Date.parse(this.from)) && !isNaN(Date.parse(this.until))){
        let fromMonthSpan1;
        let untilMonthSpan1;
        let fromMonthSpan2;
        let untilMonthSpan2;
        if(Number.parseInt(this.from.substring(1,4)) != Number.parseInt(this.until.substring(1,4))) {
          fromMonthSpan1 = this.from.substring(5,7);
          untilMonthSpan1 = 12;
          fromMonthSpan2 = 1;
          untilMonthSpan2 = this.until.substring(5,7);
        }else {
          fromMonthSpan1 = this.from.substring(5,7);
          untilMonthSpan1 = this.until.substring(5,7);
          fromMonthSpan2 = 0;
          untilMonthSpan2 = 0;
        }  
        this.findFlightsInfo(this.origin, this.dest, fromMonthSpan1, untilMonthSpan1, fromMonthSpan2, untilMonthSpan2);
      }
    });
  }

  findBestFlightInfo(orig, dest, fromMonthSpan1, untilMonthSpan1, fromMonthSpan2, untilMonthSpan2){
    
    this.service.getBestFlight(orig, dest, fromMonthSpan1, untilMonthSpan1, fromMonthSpan2, untilMonthSpan2).subscribe((data: Flight[]) => {
    if(data.length > 0){
      this.bestFlight = data[0];
      this.flightsNotFound = false;
    } 
    else{
      this.flightsNotFound = true;
    }    
    this.loading = false;
 });
 }

  findFlightsInfo(orig, dest, fromMonthSpan1, untilMonthSpan1, fromMonthSpan2, untilMonthSpan2){
     this.service.getFlights(orig, dest, fromMonthSpan1, untilMonthSpan1, fromMonthSpan2, untilMonthSpan2).subscribe((data: Flight[]) => {
      if(data.length > 0){
        this.bestFlight = data[0];
        this.worstFlight = data[data.length - 1];
        this.flightsList = data;
        this.flightsNotFound = false;
      } 
      else{
        this.flightsNotFound = true;
      }    
      this.loading = false;
  },
  error => {
    console.log(error);
    this.flightsNotFound = true;
    this.loading = false;
  } );
  }
}

