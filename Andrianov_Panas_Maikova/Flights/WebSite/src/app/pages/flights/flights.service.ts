import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {

  constructor(private http: HttpClient) { }

  apiUrl = 'http://localhost:6235/api/flights';

  getFlight() {
    // now returns an Observable of Config
    return this.http.get<Flight[]>(this.apiUrl+'/getfromdb/10');
  }

  getFlights(orig, dest, fromMonthSpan1, untilMonthSpan1, fromMonthSpan2, untilMonthSpan2) {
    // now returns an Observable of Config
    return this.http.get<Flight[]>(this.apiUrl+"/GetFlights?origin="+orig+"&destination="+dest+"&fromMonthSpan1="+fromMonthSpan1+"&UntilMonthSpan1="+untilMonthSpan1+"&fromMonthSpan2="+fromMonthSpan2+"&untilMonthSpan2="+untilMonthSpan2);
    
  }

  getBestFlight(orig, dest, fromMonthSpan1, untilMonthSpan1, fromMonthSpan2, untilMonthSpan2) {
    // now returns an Observable of Config
    return this.http.get<Flight[]>(this.apiUrl+"/GetBestFlight?origin="+orig+"&destination="+dest+"&fromMonthSpan1="+fromMonthSpan1+"&UntilMonthSpan1="+untilMonthSpan1+"&fromMonthSpan2="+fromMonthSpan2+"&untilMonthSpan2="+untilMonthSpan2);
  }
  
}

export interface Flight {
  Airline: string;
  FlightNumber: string;
  TailNumber: string;
  OriginAirport: string;
  DestinationAirport: string;  
  Stats: FlightStats;
}

export interface FlightStats {
  AvgDelay: number;
  MinDelay: number;
  MaxDelay: number;
  DelayedCount: number;
  DelayedPercent: number;
  CancelledCount: number;
  CancelledPercent: number;
  DivertedCount: number;
  DivertedPercent: number;
  TotalCount: number;
}
